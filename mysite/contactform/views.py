from django.shortcuts import render
from django.views.generic import CreateView
from .models import Person
from django.http import HttpResponse, HttpResponseRedirect

#class PersonCreateView(CreateView):
#    model = Person
#    fields = ('name', 'email', 'job_title', 'bio')

#def contactform(request):
#    return render(request, 'base.html',)


# add to the top
#from .forms import ContactForm

# add to your views
#def contactform(request):
#    form_class = ContactForm

#    return render(request, 'base.html', {
 #       'form': form_class,
#    })



class PersonCreateView(CreateView):
    model = Person
    fields = ('name', 'phone_number')
