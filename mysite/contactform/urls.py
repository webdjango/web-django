from django.conf.urls import url
from django.urls import path
from contactform.views import PersonCreateView
from . import views


urlpatterns = [
    path('contact/', PersonCreateView.as_view(success_url="/success/"), name='person_add'),
]