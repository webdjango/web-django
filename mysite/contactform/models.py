from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

class Person(models.Model):
    name = models.CharField('Ваше имя', max_length=130)
    phone_number = PhoneNumberField('Номер телефона', blank=True)

    def __str__(self):
        return u'%s %s' % (self.name, self.phone_number)