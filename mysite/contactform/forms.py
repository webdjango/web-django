from django import forms

# our new form
class ContactForm(forms.Form):
    name = forms.CharField(required=True)
    phone = forms.PhoneField(required=True)