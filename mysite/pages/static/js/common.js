	// //Попап менеджер FancyBox
	// //Документация: http://fancybox.net/howto
	// //<a class="fancybox"><img src="image.jpg" /></a>
	// //<a class="fancybox" data-fancybox-group="group"><img src="image.jpg" /></a>
	// $(".fancybox").fancybox();


	// //Каруселька
	// //Документация: http://owlgraphic.com/owlcarousel/
	// var owl = $(".carousel");
	// owl.owlCarousel({
	// 	items : 3,
	// 	autoHeight : true
	// });
	// owl.on("mousewheel", ".owl-wrapper", function (e) {
	// 	if (e.deltaY > 0) {
	// 		owl.trigger("owl.prev");
	// 	} else {
	// 		owl.trigger("owl.next");
	// 	}
	// 	e.preventDefault();
	// });
	// $(".next_button").click(function() {
	// 	owl.trigger("owl.next");
	// });
	// $(".prev_button").click(function() {
	// 	owl.trigger("owl.prev");
	// });
	
	// //Аякс отправка форм
	// //Документация: http://api.jquery.com/jquery.ajax/
	// $("#callback").submit(function() {
	// 	$.ajax({
	// 		type: "GET",
	// 		url: "mail.php",
	// 		data: $("#callback").serialize()
	// 	}).done(function() {
	// 		alert("Спасибо за заявку!");
	// 		setTimeout(function() {
	// 			$.fancybox.close();
	// 		}, 1000);
	// 	});
	// 	return false;
	// });



$(document).ready(function () {

	$('#phone-number').mask("+7 (999) 999-99-99");
	$('#phone-number-form').mask("+7 (999) 999-99-99");
	// $('a[href="#phone-form"').click(function (e) {
	// 	e.preventDefault();
	// 	var id = $(this).attr('href');
	// 	var top = $(id).offset().top;

	// 	$('body,html').animate({
	// 		scrollTop : top
	// 	},500);
	// });

	// $('.about-slider-container .slick-dots li.slick-active').next('li').css({
	// 	width: '17px',
	// 	height: '17px'
	// });
	// $('.about-slider-container .slick-dots li.slick-active').prev('li').css({
	// 	width: '17px',
	// 	height: '17px'
	// });
	// $('.about-slider-container .slick-dots li').click(function () {
	// 	$('.about-slider-container .slick-dots li').css({
	// 		width: '12px',
	// 		height: '12px'
	// 	})
	// 	$('.about-slider-container .slick-dots li.slick-active').next('li').css({
	// 		width: '17px',
	// 		height: '17px'
	// 	});
	// 	$('.about-slider-container .slick-dots li.slick-active').prev('li').css({
	// 		width: '17px',
	// 		height: '17px'
	// 	});
	// 	$('.about-slider-container .slick-dots li.slick-active').css({
	// 		width: '25px',
	// 		height: '25px'
	// 	})
	// });
	
	$('.fancy-open').fancybox();


});

$('.about-slider-container').slick({
	slidesToShow: 3,
	slidesToScroll: 1,
	dots: true,
	arrows: false,
	centerMode: true,
	focusOnSelect: true,
	autoplay: true,
	autoplaySpeed: 2000,
	responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    ]
	
});


$('.layout-slider-container').on('afterChange', function(event, slick, currentSlide){
  if (currentSlide == 0) {
	$('.slide-menu li').removeClass('active');
	$('.first-slide').parent().addClass('active');
  }
  else if(currentSlide == 1){
  	$('.slide-menu li').removeClass('active');
	$('.second-slide').parent().addClass('active');
  }
  else if(currentSlide == 2){
  	$('.slide-menu li').removeClass('active');
	$('.three-slide').parent().addClass('active');
  }
  else if(currentSlide == 3){
  	$('.slide-menu li').removeClass('active');
	$('.four-slide').parent().addClass('active');
  }
});

$('.layout-slider-container').on('beforeChange', function(event, slick, currentSlide){
  if (currentSlide == 0) {
	$('.slide-menu li').removeClass('active');
	$('.first-slide').parent().addClass('active');
  }
  else if(currentSlide == 1){
  	$('.slide-menu li').removeClass('active');
	$('.second-slide').parent().addClass('active');
  }
  else if(currentSlide == 2){
  	$('.slide-menu li').removeClass('active');
	$('.three-slide').parent().addClass('active');
  }
  else if(currentSlide == 3){
  	$('.slide-menu li').removeClass('active');
	$('.four-slide').parent().addClass('active');
  }
});


$('.first-slide').click(function (e) {
	e.preventDefault();
	$('.layout-slider-container').slick('slickGoTo',0);
	$('.slide-menu li').removeClass('active');
	$(this).parent().addClass('active');
});
$('.second-slide').click(function (e) {
	e.preventDefault();
	$('.layout-slider-container').slick('slickGoTo',1);
	$('.slide-menu li').removeClass('active');
	$(this).parent().addClass('active');
});
$('.three-slide').click(function (e) {
	e.preventDefault();
	$('.layout-slider-container').slick('slickGoTo',2);
	$('.slide-menu li').removeClass('active');
	$(this).parent().addClass('active');
});
$('.four-slide').click(function (e) {
	e.preventDefault();
	$('.layout-slider-container').slick('slickGoTo',3);
	$('.slide-menu li').removeClass('active');
	$(this).parent().addClass('active');
});

/*
$('#send-form').submit(function () {

var form_data = $(this).serialize(); //собераем все данные из формы
    $.ajax({
    type: "POST", 
    url: "mail.php",
    data: form_data,
    success: function() {
			// $('a[href="#thank-send"]').click();
			// $('#thank-send a').click(function () {
			// 	$.fancybox.close('#thank-send');
			// });
			$('#phone-number').css('font-size','20px');
			$('#phone-number').val('Форма успешно отправлена!');
			$('#phone-number').prop("disabled", true);
    	}
	});    
	return false;	
});

$('#popup-form').submit(function () {

var form_data = $(this).serialize(); //собераем все данные из формы
    $.ajax({
    type: "POST", 
    url: "mail.php",
    data: form_data,
    success: function() {
			// $('a[href="#thank-send"]').click();
			// $('#thank-send a').click(function () {
			// 	$.fancybox.close('#thank-send');
			// });
			$('#phone-number-form').css('font-size','20px');
			$('#phone-number-form').val('Форма успешно отправлена!');
			$('#phone-number-form').prop("disabled", true);
    	}
	});    
	return false;	
});

*/